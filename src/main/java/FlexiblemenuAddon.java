import io.jpress.core.addon.Addon;
import io.jpress.core.addon.AddonInfo;
import io.jpress.core.addon.AddonUtil;
import io.jpress.core.menu.MenuGroup;
import io.jpress.core.menu.MenuManager;

import java.sql.SQLException;

/**
 * anjie QQ489879492
 * 互动交流插件
 */
public class FlexiblemenuAddon implements Addon {

    @Override
    public void onInstall(AddonInfo addonInfo) {

    }


    @Override
    public void onUninstall(AddonInfo addonInfo) {

    }

    @Override
    public void onStart(AddonInfo addonInfo) {

        /**
         *  在 onStart 方法中，我们可以做很多事情，例如：创建后台或用户中心的菜单
         *
         *  此方法是每次项目启动，都会执行。
         *
         *  同时用户也可以在后台触发
         */

    }

    @Override
    public void onStop(AddonInfo addonInfo) {

        /**
         *  和 onStart 对应，在 onStart 所处理的事情，在 onStop 应该释放
         *
         *  同时用户也可以在后台触发
         */

    }
}
